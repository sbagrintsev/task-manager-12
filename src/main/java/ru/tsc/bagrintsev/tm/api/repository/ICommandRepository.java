package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.model.Command;

public interface ICommandRepository {

    Command[] getAvailableCommands();

}
